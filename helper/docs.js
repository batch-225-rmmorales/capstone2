// https://www.youtube.com/watch?v=sTLJ1mHpsOI

const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "Capstone 2.5: Simple Ecommerce",
      version: "0.0.1",
      description:
        "Learning Objectives: Express, Mongoose, Authentication, Swagger, Prisma, Svelte, Tailwind",
    },

    servers: [
      {
        url: "http://localhost:5000/",
        description: "test environment",
      },
      {
        url: "http://production.com/",
        description: "production environment",
      },
    ],
    tags: [
      {
        name: "Authentication",
        description: "Authentication",
      },
      {
        name: "User",
        description: "Create/Read/Update/Delete User Information",
      },
      {
        name: "Item",
        description: "Buy/Sell Items",
      },
      {
        name: "Message",
        description: "Comments/Chat/Notifications",
      },
      {
        name: "DevTools",
        description: "For Debug/Demo Purposes",
      },
    ],

    //   paths: {
    //     ...userRouteDoc,
    //     ...authRouteDoc,
    //   },
  },
  apis: [
    "index.js",
    "routes/userRoute.js",
    "routes/authRoute.js",
    "routes/itemRoute.js",
    "routes/messageRoute.js",
  ],
};
module.exports = swaggerOptions;
