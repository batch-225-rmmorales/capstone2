// requires
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.ObjectId,
    required: [true, "userId is required"],
  },
  products: [
    {
      productId: {
        type: mongoose.ObjectId,
        required: [true, "productId is required"],
      },
      quantity: {
        type: Number,
        default: 1,
      },
    },
  ],
  totalAmount: {
    type: Number,
  },
  isCart: Boolean,
  isOrder: Boolean,
  lastUpdated: {
    type: Date,
    // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
    default: new Date(),
  },
});

module.exports = mongoose.model("Order", orderSchema);
