// requires
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Item name is required"],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
    default: new Date(),
  },
});

module.exports = mongoose.model("Product", productSchema);
