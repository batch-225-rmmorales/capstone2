// routes/authRoute?
const express = require("express");
const authController = require("../controllers/authController");
const router = express.Router();

/**
 * @swagger
 * /auth/signup:
 *  post:
 *    tags: ["Authentication"]
 *    summary: Register to use the website 👤
 *    description: hey what does this look like
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  properties:
 *                      username:
 *                          type: string
 *                          description: display/login name used in website
 *                      email:
 *                          type: string
 *                          description: email contact of user
 *                      password:
 *                          type: string
 *                          description: password should be more than 6 characters
 *                      address:
 *                          type: string
 *                          description: delivery address
 *                      mobileNo:
 *                          type: string
 *                          description: cellphone number contact of user
 *              examples:
 *                  mors:
 *                      value:
 *                          username: mors
 *                          email: mors@mors.com
 *                          password: password
 *                          address: 123 Home
 *                          mobileNo: "+639181234567"
 *                  notmors:
 *                      value:
 *                          username: notmors
 *                          mail: mors@mors.com
 *                          password: password
 *                          address: 123 Home
 *                          mobileNo: +639181234567
 *
 *
 *
 *
 *    responses:
 *      '201':
 *        description: User successfully registered
 */
router.post("/signup", async (req, res) => {
  console.log("someone signing up");
  result = await authController.register(req);
  res.send(result);
});

/**
 * @swagger
 * /auth/signin:
 *  post:
 *    tags: ["Authentication"]
 *    summary: Login to use the website 🔑
 *    description: returns a cookie containing JWT
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  properties:
 *                      username:
 *                          type: string
 *                          description: display/login name used in website
 *                      password:
 *                          type: string
 *                          description: password should be more than 6 characters
 *              examples:
 *                  superadmin:
 *                      value:
 *                          username: superadmin
 *                          password: password
 *
 *    responses:
 *      '201':
 *        description: User successfully signed in
 */
router.post("/signin", async (req, res) => {
  result = await authController.login(req, res);
  res.send(result);
});

module.exports = router;
