const express = require("express");
const userController = require("../controllers/userController");
const { adminAuth, userAuth } = require("../controllers/authController.js");
const router = express.Router();

/**
 * @swagger
 * /api/user/reset:
 *  delete:
 *    tags: ["DevTools"]
 *    summary: Reset User Collection
 *    description: Used to reset and seed User collection from (https://jsonplaceholder.typicode.com/users)
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.delete("/reset", adminAuth, async (req, res) => {
  console.log("admin trying to delete all");
  result = await userController.reset(req);
  res.send(result);
});

/**
 * @swagger
 * /api/user:
 *  get:
 *    tags: ["User"]
 *    summary: List all users 📋
 *    description: Use to request all customers
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/", async (req, res) => {
  console.log("someone getting list of users");
  result = await userController.getAll();
  res.send(result);
});

/**
 * @swagger
 * /api/user/find:
 *  get:
 *    tags: ["User"]
 *    summary: Search by id, email or username 🔍
 *    description: use only one search parameter, if multiple are provided then priority is id > email > username
 *    parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *      - in: query
 *        name: email
 *        schema:
 *          type: string
 *      - in: query
 *        name: username
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/find", async (req, res) => {
  result = await userController.getProfile(req.query);
  res.send(result);
});

/**
 * @swagger
 * /api/user/update:
 *  put:
 *    tags: ["User"]
 *    summary: Filter and update the user collection ✏️
 *    description: Used to update user collection. Need admin privileges
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  properties:
 *                      filter:
 *                          type: object
 *                          description: filter conditions
 *                      update:
 *                          type: object
 *                          description: update object
 *              examples:
 *                  change email:
 *                      value:
 *                        filter:
 *                            email: mors@mors.com
 *                        update:
 *                            email: mors@newemail.com
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.put("/update", adminAuth, async (req, res) => {
  result = await userController.updateUser(req);
  res.send(result);
});

/**
 * @swagger
 * /api/user/delete:
 *  delete:
 *    tags: ["User"]
 *    summary: Remove a certain user by id, email or username ❌
 *    description: Used to remove a certain user. Need admin privileges
 *    parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *      - in: query
 *        name: email
 *        schema:
 *          type: string
 *      - in: query
 *        name: username
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.delete("/delete", adminAuth, async (req, res) => {
  console.log("admin trying to delete a user");
  result = await userController.delete(req.query);
  res.send(result);
});

router.put("/setadmin", adminAuth, async (req, res) => {
  let filter = req.body;
  req.body.filter = filter;
  req.body.update = { isAdmin: true };
  result = await userController.updateUser(req, res);
  res.send(result);
});

// deprecated
router.get("/:username", async (req, res) => {
  result = await userController.getProfile(req.params.username);
  res.send(result);
});

// deprecated
router.get("/:username/friends", async (req, res) => {
  result = await userController.getFriends(req.params.username);
  res.send(result);
});

module.exports = router;
