const signup = {
  tags: ["Auth"],
  description: "Register for website",

  requestBody: {
    content: {
      "application/json": {
        schema: {
          type: "object",
          properties: {
            username: {
              type: "string",
              description: "will be used as display name on site",
              example: "rmmorales",
            },
            email: {
              type: "string",
              description: "email contact information",
              example: "rmmorales@mors.com",
            },
            password: {
              type: "string",
              description: "password for authentication",
              example: "mypassword",
            },
            address: {
              type: "string",
              description: "address contact information",
              example: "123 Home",
            },
            mobileNo: {
              type: "string",
              description: "mobile contact information",
              example: "+639181234567",
            },
          },
        },
      },
    },
  },
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              message: "User successfully created",
              data: {
                username: "admin12",
                email: "admin12@mors.com",
                password: "",
                address: "123 Home",
                mobileNo: "+639181234567",
                isAdmin: true,
              },
            },
          },
        },
      },
    },
  },
};
const authRouteDoc = {
  "/auth/signup": {
    post: signup,
  },
};

module.exports = authRouteDoc;
