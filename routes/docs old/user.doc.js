const listUsers = {
  tags: ["User"],
  description: "Get all users",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              _id: "63cd6668f16f98f313470020",
              username: "admin12",
              email: "admin12@mors.com",
              password:
                "$2b$10$FrZnKyWcLqU9Maj1Vp9VjOhvjAlLBSm5s1yTqAnTMyeSCMtOycd2S",
              address: "123 Home",
              mobileNo: "+639181234567",
              isAdmin: true,
              __v: 0,
            },
          },
        },
      },
    },
  },
};
const getUserByQueryId = {
  tags: ["User"],
  summary: "Find user",
  description:
    "Find user by id, email or username in that order. Returns first match. e.g. if you want to find by username then leave id and email blank",
  parameters: [
    {
      name: "id",
      in: "query",
      description: "id of the user",
      type: "string",
      example: "63cd6668f16f98f313470020",
    },
    {
      name: "email",
      in: "query",
      description: "email of the user",
      type: "string",
      example: "user@gmail.com",
    },
    {
      name: "username",
      in: "query",
      description: "username of the user",
      type: "string",
      example: "rmmorales",
    },
  ],
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              _id: "63cd6668f16f98f313470020",
              username: "admin12",
              email: "admin12@mors.com",
              password:
                "$2b$10$FrZnKyWcLqU9Maj1Vp9VjOhvjAlLBSm5s1yTqAnTMyeSCMtOycd2S",
              address: "123 Home",
              mobileNo: "+639181234567",
              isAdmin: true,
              __v: 0,
            },
          },
        },
      },
    },
  },
};
const userRouteDoc = {
  "/api/user": {
    get: listUsers,
  },
  "/api/user/find": {
    get: getUserByQueryId,
  },
};

module.exports = userRouteDoc;
