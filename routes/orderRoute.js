const express = require("express");
const orderController = require("../controllers/orderController");
const {
  nonAdminAuth,
  adminAuth,
  userAuth,
} = require("../controllers/authController.js");

const router = express.Router();

/**
 * @swagger
 * /api/message/reset:
 *  delete:
 *    tags: ["DevTools"]
 *    summary: Reset Message Collection
 *    description: Used to reset and seed Message collection from (https://jsonplaceholder.typicode.com/users)
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.delete("/reset", adminAuth, async (req, res) => {
  console.log("admin trying to delete all");
  result = await orderController.reset(req);
  res.send(result);
});

/**
 * @swagger
 * /api/message:
 *  get:
 *    tags: ["Message"]
 *    summary: List all messages📋
 *    description: Use to request all message
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/", async (req, res) => {
  result = await orderController.getAll();
  res.send(result);
});

router.post("/create", nonAdminAuth, async (req, res) => {
  result = await orderController.createNew(req);
  res.send(result);
});

router.get("/tree/:id", userAuth, async (req, res) => {
  result = await orderController.messageTree(req, res);
  res.send(result);
});

module.exports = router;
