const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();

router.get("/", async (req, res) => {
  resultFromController = await userController.getAll();
  res.send(resultFromController);
});

router.post("/signup", async (req, res) => {
  resultFromController = await userController.createNew(req);
  res.send(resultFromController);
});

router.post("/signin", async (req, res) => {
  resultFromController = await userController.signIn(req);
  res.send(resultFromController);
});
