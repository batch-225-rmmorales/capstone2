const express = require("express");
const productController = require("../controllers/productController");
const { adminAuth, userAuth } = require("../controllers/authController.js");
const router = express.Router();

/**
 * @swagger
 * /api/item:
 *  get:
 *    tags: ["Item"]
 *    summary: List all items 📋
 *    description: Use to request all items
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/", adminAuth, async (req, res) => {
  result = await productController.getAll();
  res.send(result);
});

router.get("/active", userAuth, async (req, res) => {
  result = await productController.getActive();
  res.send(result);
});

/**
 * @swagger
 * /api/item/sell:
 *  post:
 *    tags: ["Item"]
 *    summary: Post Item for Selling in Market 🛍️
 *    description: hey what does this look like
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *              schema:
 *                  properties:
 *                      name:
 *                          type: string
 *                          description: name of item
 *                      description:
 *                          type: string
 *                          description: description of item
 *                      imgUrl:
 *                          type: string
 *                          description: image of item
 *                      price:
 *                          type: number
 *                          description: cost in pesos
 *                      status:
 *                          type: string
 *                          description: for sale / sold / archived
 *              examples:
 *                  cellphone:
 *                      value:
 *                          name: cellphone
 *                          description: cheap android smartphone
 *                          imgUrl: place-puppy.com/300x300
 *                          price: 10000,
 *                          status: "For Sale"
 *    responses:
 *      '201':
 *        description: Item successfully posted
 */
router.post("/create", adminAuth, async (req, res) => {
  result = await productController.sell(req);
  res.send(result);
});

/**
 * @swagger
 * /api/item/buy:
 *  put:
 *    tags: ["Item"]
 *    summary: Buy Item in the market 🛒
 *    description: Use query parameter id
 *    parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *    responses:
 *      '201':
 *        description: Item successfully bought
 */
router.put("/buy", userAuth, async (req, res) => {
  result = await productController.buyById(req);
  res.send(result);
});

/**
 * @swagger
 * /api/item/reset:
 *  delete:
 *    tags: ["DevTools"]
 *    summary: Reset Item Collection
 *    description: Used to reset and seed Item collection from https://dummyjson.com/products?limit=100
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.delete("/reset", adminAuth, async (req, res) => {
  console.log("admin trying to delete all");
  result = await productController.reset(req);
  res.send(result);
});

router.put("/update", userAuth, async (req, res) => {
  result = await productController.update(req);
  res.send(result);
});

router.put("/archive", userAuth, async (req, res) => {
  result = await productController.archive(req);
  res.send(result);
});

module.exports = router;
