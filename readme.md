# CAPSTONE 2.5: Simple ecommerce full stack

## Learning goals: Go full stack, use mongoose + prisma, use swagger or something similar

## nice to haves: passport js, prisma, svelte

---

## Models

- User
  - username / displayname ✅
  - email ✅
  - password ✅
  - mobileNo ✅
  - address ✅
  - isAdmin ✅
  - friends [array of userIDs] ❌
  - blocked [array of userIDs] ❌
  - reviews [] ❌
    - userID ❌
    - review ❌
    - rating ❌
  - properties ❌
    - settings ❌
      - notif ❌
      - privacy❌
- Item

  - userID ✅
  - name ✅
  - description ✅
  - price ✅
  - inventory ❌
  - likes [array of userIDs] 🤔
  - viewed [array of userIDs] ❌
  - status (for sale, sold, archived) ✅
  - iat ✅
  - properties ❌
    - negotiable ❌

- Chat / Notification

  - fromID (if from system - notification) ✅
    - notifications (friend request, message)
  - toID ✅
  - iat ✅
  - isParent ✅
  - properties❌
    - ~~status (unread, read)~~
  - message ✅
  - views [array of userIDs] ❌
  - likes [array of userIDs] ❌

- ~~Comments~~❌
  - fromID
  - toID
  - properties
    - iat
    - checkForChild (default: true)
  - message
  - likes [array of userIDs]
- Search 🤔
- External API 🤔

## Routes

- User
  - api/user/signin ✅
  - api/user/signup ✅
  - api/user/:username
  - api/user/:username/chats
  - api/user/:username/friends
  - api/user/:username/blocked
  - api/user/:username/reviews
- Item
  - api/item/create
    - auth user
  - api/item/:itemid
- Chat (can only chat with friends and not blocked)
- Comments (can only comment if not blocked)

## Controllers

- User
  - can login via username/email
- Item
- Chat
- Comments

## Backlog

- put mobile no and address in contacts
- error handling

## Resources

- https://blog.logrocket.com/jwt-authentication-best-practices/
- https://stackoverflow.com/questions/41014427/how-to-add-data-to-request-body-while-proxying-it

### Swagger

- https://www.youtube.com/watch?v=sTLJ1mHpsOI
- https://www.youtube.com/watch?v=apouPYPh_as
- https://dev.to/kabartolo/how-to-document-an-express-api-with-swagger-ui-and-jsdoc-50do

### Svelte

- https://medium.com/swlh/full-stack-development-starter-svelte-and-express-831aefee41c0

### MongoDB

- https://www.mongodb.com/developer/products/mongodb/mongodb-schema-design-best-practices/

### Tree

- https://typeofnan.dev/an-easy-way-to-build-a-tree-with-object-references/
- https://stackoverflow.com/questions/45656257/the-easiest-way-to-iterate-through-a-collection-in-mongoose
