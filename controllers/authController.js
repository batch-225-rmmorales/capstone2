// authController
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.register = async (req, res, next) => {
  const { username, password, email, mobileNo, address, isAdmin } = req.body;
  if (password.length < 6) {
    return JSON.stringify({
      message: "Password less than 6 characters",
    });
  } else if ((await User.find({ email })).length > 0) {
    return JSON.stringify({
      message: "Email account already exists",
    });
  }
  try {
    await User.create({
      email,
      password: bcrypt.hashSync(req.body.password, 10),
      isAdmin,
    });
    req.body.password = "";
    return JSON.stringify({
      message: "User successfully created",
      data: req.body,
    });
  } catch (err) {
    JSON.stringify({
      message: "User not successful created",
      error: err.message,
    });
  }
};

exports.login = async (req, res, next) => {
  try {
    const user = await User.findOne({
      email: req.body.email,
    });
    if (!user) {
      return JSON.stringify({
        message: "Login not successful",
        error: "User not found",
      });
    } else {
      const maxAge = 3 * 60 * 60; // 3 hours in seconds
      passwordCheck = bcrypt.compareSync(req.body.password, user.password);
      if (passwordCheck) {
        payload = {
          email: user.email,
          isAdmin: user.isAdmin,
          userId: user._id,
        };
        const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
          expiresIn: maxAge,
        });
        // if (!res.cookie) res.cookie = {};
        // console.log(res.cookie);

        res.cookie("jwt", accessToken, {
          httpOnly: false,
          maxAge: maxAge * 1000, // 3hrs in ms
        });
        // res.send(accessToken);

        return {
          message: "logged in successfully",
          email: user.email,
        };
      }
    }
  } catch (error) {
    return JSON.stringify({
      message: "An error occurred",
      error: error.message,
    });
  }
};

exports.adminAuth = (req, res, next) => {
  const token = req.cookies.jwt;
  if (token) {
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ message: "Not authorized" });
      } else {
        if (!decodedToken.isAdmin) {
          return res.status(401).json({ message: "Not authorized" });
        } else {
          req.body.payload = decodedToken;
          next();
        }
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
};

exports.nonAdminAuth = (req, res, next) => {
  const token = req.cookies.jwt;
  if (token) {
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ message: "Not authorized" });
      } else {
        if (decodedToken.isAdmin) {
          return res.status(401).json({ message: "Not authorized" });
        } else {
          req.body.payload = decodedToken;
          next();
        }
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
};

exports.userAuth = async (req, res, next) => {
  const token = req.cookies.jwt;
  if (token) {
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ message: "Not authorized" });
      } else {
        console.log(decodedToken);
        req.body.payload = decodedToken;
        next();
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
};
