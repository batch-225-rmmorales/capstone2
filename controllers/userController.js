// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

// const ACCESS_TOKEN_SECRET = require("../password").ACCESS_TOKEN_SECRET;
require("dotenv").config();

module.exports.reset = async () => {
  // delete all users
  await User.deleteMany();

  // create super admin
  let newUser = new User({
    username: "superadmin",
    email: "super@admin.com",
    password: bcrypt.hashSync("password", 10),
    mobileNo: "+639181234567",
    address: "123 Home",
    isAdmin: true,
  });
  await newUser.save();

  // seed user collection
  let response = await fetch("https://jsonplaceholder.typicode.com/users");
  seed = await response.json();
  console.log(seed);
  seed.forEach(async (item) => {
    newUser = new User({
      username: item.username,
      email: item.email,
      password: bcrypt.hashSync("password", 10),
      mobileNo: item.phone,
      address: item.address.street,
    });
    await newUser.save();
  });

  return JSON.stringify({ message: "User collection reset" });
};

module.exports.signup = async (req) => {
  if ((await User.find({ email: req.body.email })).length > 0) {
    return "email account already exists";
  } else {
    let newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
      mobileNo: req.body.mobileNo,
      address: req.body.address,
      isAdmin: req.body.isAdmin,
    });
    try {
      return await newUser.save();
    } catch (err) {
      return err;
    }
  }
};

module.exports.signin = async (req) => {
  let hashedPassword = await User.findOne({
    email: req.body.email,
  });
  console.log(`req.body.email is ${req.body.email}`);
  console.log(`req.body.password is ${req.body.password}`);
  console.log(`hashedPassword is ${hashedPassword}`);
  passwordCheck = bcrypt.compareSync(
    req.body.password,
    hashedPassword.password
  );
  if (passwordCheck) {
    req.body.password = hashedPassword.password;
    req.body.userID = hashedPassword._id;
    const accessToken = jwt.sign(req.body, process.env.ACCESS_TOKEN_SECRET);
    return JSON.stringify({ accessToken: accessToken });
  } else {
    return "password do not match";
  }
};

// module.exports.getProfile = async (username) => {
//   return await User.find({ username: username });
// };
module.exports.getProfile = async (query) => {
  if (!(query.id === undefined || query.id == "")) {
    return await User.find({ _id: query.id });
  } else if (!(query.email === undefined || query.email == "")) {
    return await User.find({ email: query.email });
  }
};

module.exports.delete = async (query) => {
  console.log(query);
  if (!(query.id === undefined || query.id == "")) {
    return await User.find({ _id: query.id }).remove().exec();
  } else if (!(query.email === undefined || query.email == "")) {
    return await User.find({ email: query.email }).remove().exec();
  } else if (!(query.username === undefined || query.username == "")) {
    return await User.find({ username: query.username }).remove().exec();
  }
};

module.exports.getFriends = async (username) => {
  return await User.find({ username: username }).friends;
};

module.exports.getAll = async () => {
  return await User.find({});
};

// module.exports.findEmail = async (req) => {
//   return await User.find({ email: req.body.email });
// };

// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

module.exports.updateUser = async (req) => {
  let toUpdate = await User.findOne(req.body.filter);
  return await toUpdate.update(req.body.update);
};

module.exports.setadmin = async (req, res) => {
  let toUpdate = await User.findOne(req.body);
  return await toUpdate.update({ isAdmin: true });
};

// module.exports.updateByID = async (taskID, req) => {
//   let toUpdate = await Task.findOne({ _id: taskID });
//   console.log(req.body.update);
//   await toUpdate.update(req.body.update);
//   return toUpdate.save();
// };
