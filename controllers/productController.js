// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Product = require("../models/product");

module.exports.getAll = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Product.find({}).sort({ $natural: -1 });
};

module.exports.getActive = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Product.find({ isActive: true }).sort({ $natural: -1 });
};

module.exports.sell = async (req) => {
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return await newProduct.save();
};

module.exports.buyById = async (req) => {
  let toUpdate = await Product.findOne({ _id: req.query.id });
  return await toUpdate.update({
    buyer: req.body.payload.username,
    status: "Sold",
  });
};

module.exports.archive = async (req) => {
  let toUpdate = await Product.findOne({ _id: req.body.productid });
  return await toUpdate.update({
    isActive: false,
  });
};

module.exports.update = async (req) => {
  let toUpdate = await Product.findOne(req.body.filter);
  return await toUpdate.update(req.body.update);
};

module.exports.reset = async () => {
  // delete all users
  await Item.deleteMany();

  // seed user collection
  let response = await fetch("https://dummyjson.com/products?limit=100");
  preseed = await response.json();
  seed = preseed.products;
  console.log(seed);
  usernames = [
    "Bret",
    "Samantha",
    "Antonette",
    "Elwyn.Skiles",
    "Maxime_Nienow",
    "Leopoldo_Corkery",
    "Delphine",
    "Moriah.Stanton",
    "Kamren",
    "Karianne",
  ];
  seed.forEach(async (item) => {
    var randomUser = usernames[Math.floor(Math.random() * usernames.length)];
    newItem = new Product({
      seller: randomUser,
      name: item.title,
      description: item.description,
      imgUrl: item.thumbnail,
      price: item.price * 60,
      status: "For Sale",
    });
    await newItem.save();
  });

  return JSON.stringify({ message: "User collection reset" });
};
