// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Course = require("../models/course");

module.exports.getAll = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Course.find({});
};

module.exports.createNew = async (req) => {
  let newTask = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    isActive: req.body.isActive,
    enrollees: req.body.enrollees,
  });

  return await newTask.save();
};

async function updateGeneral(filter, update) {
  let toUpdate = await Course.findOne(filter);
  await toUpdate.update(update);
  return toUpdate.save();
}

module.exports.archiveByID = async (courseID) => {
  return updateGeneral({ _id: courseID }, { isActive: false });
};

module.exports.updateByID = async (courseID, update) => {
  return updateGeneral({ _id: courseID }, update);
};

// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

// module.exports.updateTask = async (req) => {
//   let toUpdate = await Task.findOne(req.body.filter);
//   return await toUpdate.update(req.body.update);
// };
