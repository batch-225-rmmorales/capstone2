// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const User = require("../models/user");
const jwt = require("jsonwebtoken");
// const ACCESS_TOKEN_SECRET = require("../password").ACCESS_TOKEN_SECRET;
require("dotenv").config();

module.exports.signIn = async (req) => {
  const accessToken = jwt.sign(req.body, process.env.ACCESS_TOKEN_SECRET);
  return JSON.stringify({ accessToken: accessToken });
};

module.exports.getAll = async () => {
  return await User.find({});
};

// module.exports.findEmail = async (req) => {
//   return await User.find({ email: req.body.email });
// };

module.exports.createNew = async (req) => {
  if ((await User.find({ email: req.body.email })).length > 0) {
    return "email account already exists";
  } else {
    let newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      isAdmin: req.body.isAdmin,
      mobileNo: req.body.mobileNo,
      enrollments: req.body.enrollments,
    });
    try {
      return await newUser.save();
    } catch (err) {
      return err;
    }
  }
};

// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

// module.exports.updateTask = async (req) => {
//   let toUpdate = await Task.findOne(req.body.filter);
//   return await toUpdate.update(req.body.update);
// };

// module.exports.updateByID = async (taskID, req) => {
//   let toUpdate = await Task.findOne({ _id: taskID });
//   console.log(req.body.update);
//   await toUpdate.update(req.body.update);
//   return toUpdate.save();
// };
