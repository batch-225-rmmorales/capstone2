// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Order = require("../models/order");
const User = require("../models/user");

module.exports.getAll = async () => {
  return await Order.find({});
};

module.exports.createNew = async (req) => {
  req.body.userId = req.body.payload.userId;
  console.log(req.body);
  req.body.totalAmount = 0;
  req.body.isCart = true;
  req.body.isOrder = false;

  // req.body.totalAmount =  req.body.products.reduce((r, {cost,quantity}) => r+= cost*quantity, 0)
  let newOrder = new Order(req.body);
  console.log(newOrder.populate("products").products);

  return await newOrder.save();
};
