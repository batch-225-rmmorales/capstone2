const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const authRoute = require("./routes/authRoute");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerOptions = require("./helper/docs");
const { SwaggerTheme } = require("swagger-themes");

require("dotenv").config();

const app = express();

const port = 5000;
// CORS (Cross-Origin Resource Sharing) is a security feature implemented by web browsers that blocks web pages from making requests to a different domain than the one that served the web page. This is done to prevent malicious websites from stealing sensitive information from other websites
//In Node.js, CORS can be implemented using a middleware package such as cors. It allows you to configure the server to allow or disallow specific origins and HTTP methods. This can be useful if you are building a web application that makes API calls to a server running on a different domain.
app.use(cors());
app.use(cookieParser());
app.use(express.json());

//swagger
const theme = new SwaggerTheme("v3");
const options = {
  explorer: true,
  customCss: theme.getBuffer("dark"),
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs, options));
app.use(express.urlencoded({ extended: true }));

let db = mongoose.connection;
mongoose.connect(
  `mongodb+srv://mors2:${process.env.PASSWORD}@cluster0.uvgjod2.mongodb.net/cap-backend?retryWrites=true&w=majority`,
  //options in braces
  {
    // In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB
    useNewUrlParser: true,

    // False by default. Set to true opt in to using MongoDB driver's new connection management engine. You shoult set this option to true, except for the unlikely case that it prevents you from mainting a stable connection
    useUnifiedTopology: true,
  }
);

// Setup notification for connection success or failure

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("connected to mongodb"));

/**
 * @swagger
 * /customers:
 *  get:
 *    description: Use to request all customers
 *    responses:
 *      '200':
 *        description: A successful response
 */
app.use("/user", userRoute);
app.use("/product", productRoute);
app.use("/order", orderRoute);
app.use("/auth", authRoute);

app.listen(port, () => console.log(`listening port ${port}`));
