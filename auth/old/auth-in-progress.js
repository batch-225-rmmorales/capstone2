require("dotenv").config();
const User = require("../models/user");

// https://stackoverflow.com/questions/42972076/how-to-pass-forward-a-post-req-to-another-api-get-the-res-and-send-it-back
const request = require("request");

module.exports.authenticateToken = async (req, res, next) => {
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];
  
    // no authentication token
    if (token == null) return res.sendStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      if (err) {
        // token not verified
        return res.sendStatus(403);
      }
      //check if authorized
      req.user = user;
      //https://stackoverflow.com/questions/32633561/cant-access-object-property-of-a-mongoose-response
      console.log(req.user.password);
    });
    let myUser = await User.findOne({
      email: req.user.email,
    });
    console.log(myUser.password);
  
    let passwordCheck = myUser.password == req.user.password;
    console.log(passwordCheck);
    if (passwordCheck) {
        res.send(user)
      next();
    } else {
      //wrong status password dont match
      return res.sendStatus(401);
    }
  }
  